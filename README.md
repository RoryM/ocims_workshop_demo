# ocims_workshop_demo

Sets up DB environment for OCIMS workshop and populates with a couple shape files and dummy AIS data. 

Have a look at the .env file and configure ports as desired. 

Navigate to the docker-compose.yaml folder in your terminal and run 
> docker-compose up

This should download container images, bring them up and populate the DB with some example data.

You can test by opening PG admin (localhost:5050 as default) and adding a new server with the hostname "DB" and port "5432" (remember that the PG Admin is on the docker network and so can see the other containers on the shared docker network, if you tried to access this from outside the docker network eg: from PGadmin running on bare metal, then you should use your host machine's IP and the port 6543)

You can also link QGIS to the postgres database by pointing it to hostmachine:6543