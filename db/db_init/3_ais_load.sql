-----------------------------------------------------------------------
-- The user and DB is handled by the docker environment variables
--BEGIN;
--CREATE USER oceanmapper with encrypted password 'ocean_pw';
--COMMIT;


-----------------------------------------------------------------------
-- pos_reports holds AIS position reports
-- MMSI,BaseDateTime,LAT,LON,SOG,COG,Heading,VesselName,IMO,CallSign,VesselType,Status,Length,Width,Draft,Cargo
-- 366874980,2017-01-19T03:40:27,29.98052,-93.87640,0.0,-138.5,276.0,HERNDON R,IMO8644498,WDB2352,1025,under way using engine,19.99,7.3,,31

CREATE TABLE ais.sat_data_2019_12
(
    mmsi text COLLATE pg_catalog."default" NOT NULL,
    navigation_status character varying(2) COLLATE pg_catalog."default",
    rot smallint,
    sog numeric(4,1),
    longitude double precision NOT NULL,
    latitude double precision NOT NULL,
    "position" geometry(Point,4326),
    cog numeric(4,1),
    hdg numeric(4,1),
    event_time timestamp with time zone NOT NULL,
    server_time timestamp with time zone NOT NULL,
    msg_type character varying(2) COLLATE pg_catalog."default",
    data_source character varying(4) COLLATE pg_catalog."default",
    routing_key text COLLATE pg_catalog."default",
    id serial,
    CONSTRAINT login_pos_report_pkey PRIMARY KEY (id)
);

-- Index: login_pos_report_event_time_idx

-- DROP INDEX public.login_pos_report_event_time_idx;

CREATE INDEX login_pos_report_event_time_idx
    ON ais.sat_data_2019_12 USING btree
    (event_time)
    TABLESPACE pg_default;

-- Index: login_pos_report_mmsi_idx

-- DROP INDEX public.login_pos_report_mmsi_idx;

CREATE INDEX login_pos_report_mmsi_idx
    ON ais.sat_data_2019_12 USING btree
    (mmsi COLLATE pg_catalog."default")
    TABLESPACE pg_default;


COPY ais.sat_data_2019_12 (mmsi,navigation_status,rot,sog,longitude,latitude,position,cog,hdg,event_time,server_time,msg_type,data_source,routing_key,id)
FROM '/tmp/ais_1M.csv' DELIMITER ',' CSV HEADER;


-----------------------------------------------------------------------
-- Load the data, creat the geom column and then index it
-----------------------------------------------------------------------
ALTER TABLE ais.sat_data_2019_12 ADD COLUMN geom geometry(Point, 4326);
UPDATE ais.sat_data_2019_12 SET geom=st_SetSrid(st_MakePoint(longitude, latitude), 4326);

CREATE INDEX ais_ves_pos_2019_12
    ON ais.sat_data_2019_12 USING gist
    ("position")
    TABLESPACE pg_default;

 
 
COMMIT;
-----------------------------------------------------------------------

