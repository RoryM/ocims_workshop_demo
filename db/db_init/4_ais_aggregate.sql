-----------------------------------------------------------------------
-- This file works to aggregte data over pixels specified in the grid.
-- STEPS:
--	1- Aggregate AIS data so 1 row per MMSI showing vessel classes and sizes 
--	2- Build window on vessels: this time/speed/pos and previous time/speed/pos per mmsi
--	3- Find gaps in data feed (is this neccesary for sat-ais?)
--	4- Aggregate over grid using vessel lead/lag window
--	5- create raster from aggregate
--	6- ???
--  7- Profit


-----------------------------------------------------------------------
-- 1: Mat view of AIS  aggregate
/*
CREATE MATERIALIZED VIEW ais_ship_detials 
AS
SELECT DISTINCT ON (mmsi) 
mmsi,
basedatetime,
vesselname,
imo,
callsign,
vesseltype,
length,
width,
cargo
FROM ais.marinecadastre
ORDER BY 
mmsi, basedatetime desc

CREATE INDEX marinecadastre_event_time
    ON ais.marinecadastre USING btree
    (BaseDateTime);
    
CREATE INDEX marinecadastre_mmsi_idx
    ON ais.marinecadastre USING btree
    (mmsi COLLATE pg_catalog."default");

CREATE TRIGGER marinecadastre_event_time_partition_insert_trigger
    BEFORE INSERT
    ON ais.pos_reports
    FOR EACH ROW
    EXECUTE PROCEDURE ais.create_partition_and_insert();


-----------------------------------------------------------------------
-- Load the data, creat the geom column and then index it
-----------------------------------------------------------------------
ALTER TABLE ais.marinecadastre ADD COLUMN geom geometry(Point, 4326);
UPDATE ais.marinecadastre SET geom=st_SetSrid(st_MakePoint(lon, lat), 4326);

CREATE INDEX marinecadastre_pos
    ON ais.marinecadastre USING gist
    (position);
 
COMMIT;
-----------------------------------------------------------------------
*/

